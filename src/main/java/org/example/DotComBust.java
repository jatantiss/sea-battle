package org.example;

import java.util.ArrayList;

public class DotComBust {
    // Обхявляем и инициализурем переменные которые нам понадобятся.
    private GameHelper helper = new GameHelper();
    public ArrayList<DotCom> dotComList = new ArrayList<DotCom>();
    private int numOfGuesses = 0;

    private void setUpGame(){
        // Создаем три обхекта DotCom, даем им имена и помещаем в ArrayList
        DotCom one = new DotCom();
        one.setName("Pets.com");
        DotCom two = new DotCom();
        two.setName("eToys.com");
        DotCom three = new DotCom();
        three.setName("Go2.com");
        dotComList.add(one);
        dotComList.add(two);
        dotComList.add(three);

        // Выводим краткие инструкции для пользователя.
        System.out.println("Ваша цель потопить три сайта");
        System.out.println("Pets.com, eToys.com, Go2.com");
        System.out.println("Потомпите за минимальное кол.во ходов");

        // Повторяем с каждым объектом DotCOm
        for (DotCom dotComToSet : dotComList) {
            // Запрашиваем у вспомагательного объекта адрес сайта.
            ArrayList<String> newLocation = helper.placeDotCom(3);
            // Вызываем сеттер из объекта DotCom,
            // чтоб передать ему местоположение которое только что получили от вспомагательного объекта
            dotComToSet.setLocationCells(newLocation);
        }
    }
    void startPlaying(){
        // До тех пор пока список объектов не станет пустым
        while (!dotComList.isEmpty()){
            // Получаем вользовательский ввод.
            String userGuess = helper.getUserInput("Сделайте ход");
            // Вызываем наш метод checkUserGuess.
            checkUserGuess(userGuess);
        }
        // Вызываем наш метод finishGame.
        finishGame();
    }
    void checkUserGuess(String userGuess){
        // Инкрементируем количество попыток, которые сделал пользователь.
        numOfGuesses++;
        // Подразумеваем промах пока не выяснили обратного.
        String result = "Мимо";

        // Повторяаем это для всех объектов DotCom в списке.
        for (DotCom dotComToTest : dotComList){
            // Просим DotCom проверить ход пользователя, ищем попадание (или потопление).
            result = dotComToTest.checkYourSelf(userGuess);
            if (result.equals("Попал")){
                // Выбираемся из цикла раньше времени, нет смысла проверять другие сайты.
                break;
            }

            if (result.equals("Потопил")){
                // Ему пришел конец, так что удаляем его из списка сайтов и выходим из цикла.
                dotComList.remove(dotComToTest);
                break;
            }
        }
        // Выводим для пользователя результат.
        System.out.println(result);
    }
    void finishGame(){
        // Выводить сообщение о тои как пользователь прошел игру.
        System.out.println("Вы полопили всё за "+ numOfGuesses + " попыток");
    }

    public static void main(String[] args) {
        // Создаем игровой объект.
        DotComBust game = new DotComBust();
        // Говорим оигровому объекту подготовить игру.
        game.setUpGame();
        // Говорим игровому объекту начать гравный игровой цикл, продолжаем запрашивать пользовательский ввод и проверять полученные данные.
        game.startPlaying();
    }
}
